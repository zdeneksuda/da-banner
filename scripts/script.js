var elm, missingElement;


window.addEventListener('load', function () {
    elm = document.createElement("div"); // Creates an element to test css animation support
    missingElement = 'Missing elements in DOM: '; // Static part of an error message
    
    run(); // Start animation
});

/**
 *
 * @param element - gets a DOM element selector
 * @returns {any} - 1. DOM element or 2. error message
 */
function getElement(element) {
    if (document.querySelector(element) !== null) {
        return document.querySelector(element); // 1.
    } else {
        console.error(`${missingElement} ${element}`); // 2.
    }
}

/**
 *
 * @param elem
 * @param anim
 */
function animate(elem, anim) {
    elem.style.animation = anim; // Sets inline CSS animation to DOM element
}

function run() {
    if (elm.style.animationName !== undefined) { // checks if css animation is supported
    
        var phase = 0;
        
        function timeline() {
            phase++;
            
            // Default setting for DOM elements
            getElement('.frame--2').style.transform = 'translate3d(100%, 0, 0)';
            getElement('.frame--end').style.transform = 'translate3d(100%, 0, 0)';
    
            switch (phase) {
                case 1:
                    animate(getElement('.frame--1 .animWrapper--1'), 'swipeIn 0.5s ease-out forwards');
                    setTimeout(timeline, 2000);
                    break;
                    
                case 2:
                    animate(getElement('.frame--1 .animWrapper--1'), 'fadeOut 0.25s ease-out forwards');
                    animate(getElement('.frame--1 .animWrapper--2'), 'swipeIn 0.5s ease-out forwards');
                    setTimeout(timeline, 2000);
                    break;
                    
                case 3:
                    animate(getElement('.frame--1'), 'driveOut 0.5s ease-out forwards');
                    animate(getElement('.frame--2'), 'driveIn 0.5s ease-out forwards');
                    setTimeout(timeline, 2000);
                    break;
                    
                case 4:
                    animate(getElement('.frame--2 .animWrapper--1'), 'swipeOut 0.25s ease-out forwards');
                    animate(getElement('.frame--2 .animWrapper--2'), 'fadeIn 0.5s ease-out forwards');
                    setTimeout(timeline, 500);
                    break;
                    
                case 5:
                    animate(getElement('.principle--2'), 'fadeIn 0.25s ease-out 0.1s forwards');
                    animate(getElement('.principle--3'), 'fadeIn 0.25s ease-out 0.15s forwards');
                    animate(getElement('.principle--4'), 'fadeIn 0.25s ease-out 0.2s forwards');
                    animate(getElement('.principle--5'), 'fadeIn 0.25s ease-out 0.25s forwards');
                    animate(getElement('.principle--6'), 'fadeIn 0.25s ease-out 0.3s forwards');
                    animate(getElement('.principle--7'), 'fadeIn 0.25s ease-out 0.35s forwards');
                    animate(getElement('.principle--8'), 'fadeIn 0.25s ease-out 0.4s forwards');
                    animate(getElement('.principle--9'), 'fadeIn 0.25s ease-out 0.45s forwards');
                    animate(getElement('.principle--10'), 'fadeIn 0.25s ease-out 0.5s forwards');
                    setTimeout(timeline, 2000);
                    break;
    
                case 6:
                    animate(getElement('.frame--2'), 'driveOut 0.5s ease-out forwards');
                    animate(getElement('.frame--end'), 'driveIn 0.5s ease-out forwards');
                    setTimeout(timeline, 2000);
                    break;
                    
                default:
                    break;
            }
        }
        
        timeline();
        
    } else {
        // fallback - show last frame
        document.querySelector('.frame--1').style.display = 'none';
        document.querySelector('.frame--2').style.display = 'none';
    }
}