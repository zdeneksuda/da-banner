# DEMO banner

### Dobrovolná úloha

1. Optimalizujte datovou velikost banneru   
Nápověda: zaměřte se na obrázky. A zjistěte si, [jak svůj projekt můžete posunou na další level](https://docs.google.com/document/d/1defOi2rcnwD__8kKG6ZoGhiF9S6A4gOsVTPizkI2NKo/edit#heading=h.xrua3h2pgw9q).  

2. Nahraďte v Javascriptu vykreslování postaviček (.principle) jednou funkcí  
Nápověda: Javascript nabízí různé [smyčky a iterace](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration). Zvažte jejich možnost použití.  
[Zobrazit řešení 2. úkolu](https://docs.google.com/document/d/1defOi2rcnwD__8kKG6ZoGhiF9S6A4gOsVTPizkI2NKo/edit#heading=h.w93fh8wzm59o)
3. Přidejte slide podle náhledu uvedeného v prezentaci do posledního framu  
Nápověda: jako vzor vám může posloužit první frame a [tato ověřená metoda](https://www.youtube.com/watch?v=QOJzcGaBFPE)   
[Zobrazit řešení 3. úkolu](https://docs.google.com/document/d/1defOi2rcnwD__8kKG6ZoGhiF9S6A4gOsVTPizkI2NKo/edit#heading=h.xw6mlh5h030t)

### Odevzdání
- nahrajte, prosím, zazipovaný balíček (pojmenovaný ve formátu "digitalni-akademie_JMENO-PRIJMENI.zip") na gDrive Digitální akademie